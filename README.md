# Javascript Assignment 1
This assigment is part of Java Fullstack bootcamp held by Noroff.

## Background
The task was to create simple computer shop website by using HTML, CSS and plain JavaSricpt.

## Requirements
Website creation had 3 phases.

First phase included bank functionality. Website should show bank balance and there shuold be loand button.
Buttons function was to get loan but loan can't be bigger than two times the current bank balance.
Also if you got a loan, you can't get a new loan until the old one is paid off.

Second phase was work functionality. Which includes button for work, this increases work balance by 100 each time it is clicked.
There is also bank button, which transfer money from work balance to bank balance with a catch.
If you got a loan the transfer shuold include 10% cut for the loan payment.
Also with loan you have option to pay your work balance towards the loan by pressing pay loan button.

Lastly there is the computer section. Each computer is fetched from the api to website. There is the buy button to buy a computer which checks if your bank balance includes enough money and therefore you even get a message for getting new computer or message that you don't got enough money.

## Usage
Clone reporitory to your machine and with help of VS server, you can run the index.html file in your browser at localhost ip.

## Maintainers
joniko aka Joni Kokko
