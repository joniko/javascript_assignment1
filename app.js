const computersElement = document.getElementById("computers");
const computerTitleElement = document.getElementById("title");
const computersDescriptionElement = document.getElementById("description");
const computersPriceElement = document.getElementById("price");
const computersImageElement = document.getElementById("image");
const computersFeaturesElement = document.getElementById("features");

const bankBalanceElement = document.getElementById("bankBalance");
const workBalanceElement = document.getElementById("workBalance");
const loanBalanceElement = document.getElementById("loanBalance");
const loanTotalElement = document.getElementById("loanTotal");

const loanButtonElement = document.getElementById("loan-btn");
const bankButtonElement = document.getElementById("bank-btn");
const workButtonElement = document.getElementById("work-btn");
const repayLoanButtonElement = document.getElementById("repayloan-btn");
const buyButtonElement = document.getElementById("buy-btn");


let computers = [];
let loanAmount = 0;
let workAmount = 0;
let bankAmount = 0;
let computerPrice = 0;
let flag = true;

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToMenu(computers));

const addComputersToMenu = (computers) => {
    computers.forEach(c => addComputerToMenu(c));
}

const addComputerToMenu = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(computerElement);
    beginningFunction();
}

const beginningFunction = () => {
    bankBalanceElement.innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(`${bankAmount}`);
    workBalanceElement.innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(`${workAmount}`);
    loanBalanceElement.style.visibility = "hidden";
    repayLoanButtonElement.style.visibility = "hidden"; 
}

function createComputerSpec(specs){
    const ul = computersFeaturesElement;
    if (ul) {
      while (ul.firstChild) {
        ul.removeChild(ul.firstChild);
      }
    }

    for (let i in specs) {
        const spec = specs[i];
        const li = document.createElement('li');
        li.innerText = spec;  
        computersFeaturesElement.appendChild(li);
    }
}


const handleComputerMenuChange = e => {
    const selectedComputer = computers[e.target.selectedIndex];
    computerTitleElement.innerText = selectedComputer.title;
    computersDescriptionElement.innerText = selectedComputer.description;
    computersPriceElement.innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(`${selectedComputer.price}`);
    computersImageElement.src = `https://noroff-komputer-store-api.herokuapp.com/${selectedComputer.image}`;
    createComputerSpec(selectedComputer.specs);
    computerPrice = parseFloat(selectedComputer.price);
    flag = false;
}

computersElement.addEventListener("change", handleComputerMenuChange);


//work button functionality
const handleWorkButton = () => {
    workAmount += 100
    workBalanceElement.innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(`${workAmount}`);
}

workButtonElement.addEventListener("click", handleWorkButton);


//bank button functionality
const handleBankButton = () => {
    if(loanAmount == 0){
        bankAmount += workAmount; 
    }
    else{
        if((workAmount*0.1) <= loanAmount){
            loanAmount -= (workAmount*0.1);
            bankAmount += (workAmount-workAmount*0.1);
        }
        else{
            let overpay = (workAmount*0.1) - loanAmount;
            bankAmount += overpay;
            loanAmount = 0;
        }
    }
    workAmount = 0;
    if(loanAmount == 0){
        loanBalanceElement.style.visibility = "hidden";
        repayLoanButtonElement.style.visibility = "hidden";
        loanButtonElement.style.visibility = "visible";
    }
    workBalanceElement.innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(`${workAmount}`);
    bankBalanceElement.innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(`${bankAmount}`);
    loanTotalElement.innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(`${loanAmount}`);
}

bankButtonElement.addEventListener("click", handleBankButton);


//repay button functionality
const handleRepayButton = () => {
    if(loanAmount >= workAmount){
        loanAmount -= workAmount;
    }
    else{
        let overpay = workAmount - loanAmount;
        bankAmount += overpay;
        loanAmount = 0;
    }
    workAmount = 0;
    if(loanAmount == 0){
        loanBalanceElement.style.visibility = "hidden";
        repayLoanButtonElement.style.visibility = "hidden";
        loanButtonElement.style.visibility = "visible";
    }
    workBalanceElement.innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(`${workAmount}`);
    bankBalanceElement.innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(`${bankAmount}`);
    loanTotalElement.innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(`${loanAmount}`);    
}

repayLoanButtonElement.addEventListener("click", handleRepayButton);

//get loan button functionality
const handleLoanButton = () => {
    const wantedLoan = prompt("Enter the amount you want to loan: ");
    if(wantedLoan > 0 && wantedLoan <= (bankAmount*2)){
        loanAmount+= parseFloat(wantedLoan);
        bankAmount += parseFloat(wantedLoan);
        loanButtonElement.style.visibility = "hidden";
        loanBalanceElement.style.visibility = "visible";
        repayLoanButtonElement.style.visibility = "visible";
        bankBalanceElement.innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(`${bankAmount}`);
        loanTotalElement.innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(`${bankAmount}`);

    }
    else{
        alert("You cant have more than 2 times of your current bank amount into loan");
    }

}

loanButtonElement.addEventListener("click", handleLoanButton);

//buy button functionality
const handleBuyButton = () => {
    if(bankAmount >= computerPrice && flag==false){
        bankAmount -= computerPrice;
        alert("You are now the owner of the new laptop!");
        bankBalanceElement.innerText = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(`${bankAmount}`);
    }

    else{
        if(flag==true){
            alert("Select a laptop");
        }
        else{
            alert("You cannot afford the laptop!");
        }
    }

}

buyButtonElement.addEventListener("click", handleBuyButton);
